﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SimpleIO;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {

            const uint mcp2200_VID = 0x04D8;
            const uint mcp2200_PID = 0x00DF;


            SimpleIOClass.InitMCP2200(mcp2200_VID, mcp2200_PID);  // initialize the USB DEVICE
            
              InitializeComponent(); // initialize the form
        }

        private void button1_Click(object sender, EventArgs e)
        {
            uint on = 0xff;
            uint off = 0xAA;
            bool isConnected = false;
            isConnected = SimpleIOClass.IsConnected();
            
            if (isConnected == true)
            {
                SimpleIOClass.SetPin(5);
                MessageBox.Show("We're connected");
            }
            else
            {
                MessageBox.Show("We're NOT connected");
            }
            
        }

    }
}
