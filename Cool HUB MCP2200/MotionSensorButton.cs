﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SimpleIO;


namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        string RxString;


        public Form1()
        {

            const uint mcp2200_VID = 0x04D8;
            const uint mcp2200_PID = 0x00DF;


            SimpleIOClass.InitMCP2200(mcp2200_VID, mcp2200_PID);  // initialize the USB DEVICE
            
              InitializeComponent(); // initialize the form
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //uint on = 0xff;
            //uint off = 0xAA;
            bool isConnected = false;
            isConnected = SimpleIOClass.IsConnected();
            
            if (button1.Text == "Click to Enable")
            {
                button1.Text = "ON and Armed";

                SimpleIOClass.SetPin(3);

            }
            else
            {
                button1.Text = "Click to Enable";
                SimpleIOClass.ClearPin(3);
            }
            
        }


        private void button2_Click_1(object sender, EventArgs e)
        {
            bool isConnected = false;
            isConnected = SimpleIOClass.IsConnected();

            if (button2.Text == "BUZZER OFF")
            {
                button2.Text = "BUZZER ON";

                SimpleIOClass.SetPin(4);

            }
            else
            {
                button2.Text = "BUZZER OFF";
                SimpleIOClass.ClearPin(4);
            }
        }

        private void buttonStart_Click(object sender, EventArgs e)
        {
            serialPort1.PortName = "COM3";
            serialPort1.BaudRate = 19200;

            serialPort1.Open();
            if (serialPort1.IsOpen)
            {
                buttonStart.Enabled = false;
                buttonStop.Enabled = true;
                textBox1.ReadOnly = false;
            }
        }

        private void buttonStop_Click(object sender, EventArgs e)
        {
           
           
            if (serialPort1.IsOpen)
            {
                char[] buff = new char[1];

                // Load element 0 with the key character.

                buff[0] ='1';

                // Send the one character buffer.
                serialPort1.Write(buff, 0, 1);
            }
        }
        private void button3_Click(object sender, EventArgs e)
        {

            if (serialPort1.IsOpen)
            {
                char[] buff = new char[1];

                // Load element 0 with the key character.

                buff[0] = '2';

                // Send the one character buffer.
                serialPort1.Write(buff, 0, 1);
            }
        }
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (serialPort1.IsOpen) serialPort1.Close();
        }
        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            // If the port is closed, don't try to send a character.

            if (!serialPort1.IsOpen) return;

            // If the port is Open, declare a char[] array with one element.
            char[] buff = new char[1];

            // Load element 0 with the key character.

            buff[0] = e.KeyChar;

            // Send the one character buffer.
            serialPort1.Write(buff, 0, 1);

            // Set the KeyPress event as handled so the character won't
            // display locally. If you want it to display, omit the next line.
            //e.Handled = true;
        }
  
        private void DisplayText(object sender, EventArgs e)
        {
            textBox1.AppendText(RxString);
        }
  
        private void serialPort1_DataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            RxString = serialPort1.ReadExisting();
            this.Invoke(new EventHandler(DisplayText));
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }





    }
}
