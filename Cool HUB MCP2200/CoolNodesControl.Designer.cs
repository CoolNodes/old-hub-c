﻿namespace WindowsFormsApplication1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.button2 = new System.Windows.Forms.Button();
            this.buttonStart = new System.Windows.Forms.Button();
            this.node1 = new System.Windows.Forms.Button();
            this.serialPort1 = new System.IO.Ports.SerialPort(this.components);
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.node2 = new System.Windows.Forms.Button();
            this.node3 = new System.Windows.Forms.Button();
            this.node4 = new System.Windows.Forms.Button();
            this.node5 = new System.Windows.Forms.Button();
            this.node6 = new System.Windows.Forms.Button();
            this.node7 = new System.Windows.Forms.Button();
            this.node8 = new System.Windows.Forms.Button();
            this.node9 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.button16 = new System.Windows.Forms.Button();
            this.button17 = new System.Windows.Forms.Button();
            this.button18 = new System.Windows.Forms.Button();
            this.button20 = new System.Windows.Forms.Button();
            this.GetSensorReading = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button19 = new System.Windows.Forms.Button();
            this.button21 = new System.Windows.Forms.Button();
            this.button22 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.button2.Location = new System.Drawing.Point(255, 3);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(102, 35);
            this.button2.TabIndex = 1;
            this.button2.Text = "BUZZER ON";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // buttonStart
            // 
            this.buttonStart.Location = new System.Drawing.Point(133, 3);
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Size = new System.Drawing.Size(95, 41);
            this.buttonStart.TabIndex = 3;
            this.buttonStart.Text = "Start";
            this.buttonStart.UseVisualStyleBackColor = true;
            this.buttonStart.Click += new System.EventHandler(this.buttonStart_Click);
            // 
            // node1
            // 
            this.node1.Location = new System.Drawing.Point(527, 5);
            this.node1.Name = "node1";
            this.node1.Size = new System.Drawing.Size(86, 33);
            this.node1.TabIndex = 4;
            this.node1.Text = "Node 1";
            this.node1.UseVisualStyleBackColor = true;
            this.node1.Click += new System.EventHandler(this.buttonStop_Click);
            // 
            // serialPort1
            // 
            this.serialPort1.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(this.serialPort1_DataReceived);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(122, 56);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(399, 449);
            this.textBox1.TabIndex = 5;
            this.textBox1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox1_KeyPress);
            // 
            // node2
            // 
            this.node2.Location = new System.Drawing.Point(527, 44);
            this.node2.Name = "node2";
            this.node2.Size = new System.Drawing.Size(86, 34);
            this.node2.TabIndex = 6;
            this.node2.Text = "Node 2";
            this.node2.UseVisualStyleBackColor = true;
            this.node2.Click += new System.EventHandler(this.button3_Click);
            // 
            // node3
            // 
            this.node3.Location = new System.Drawing.Point(527, 84);
            this.node3.Name = "node3";
            this.node3.Size = new System.Drawing.Size(86, 35);
            this.node3.TabIndex = 7;
            this.node3.Text = "Node 3";
            this.node3.UseVisualStyleBackColor = true;
            this.node3.Click += new System.EventHandler(this.button3_Click_1);
            // 
            // node4
            // 
            this.node4.Location = new System.Drawing.Point(527, 126);
            this.node4.Name = "node4";
            this.node4.Size = new System.Drawing.Size(86, 37);
            this.node4.TabIndex = 9;
            this.node4.Text = "Node 4";
            this.node4.UseVisualStyleBackColor = true;
            this.node4.Click += new System.EventHandler(this.button4_Click);
            // 
            // node5
            // 
            this.node5.Location = new System.Drawing.Point(527, 169);
            this.node5.Name = "node5";
            this.node5.Size = new System.Drawing.Size(86, 37);
            this.node5.TabIndex = 10;
            this.node5.Text = "Node 5";
            this.node5.UseVisualStyleBackColor = true;
            this.node5.Click += new System.EventHandler(this.button5_Click);
            // 
            // node6
            // 
            this.node6.Location = new System.Drawing.Point(527, 212);
            this.node6.Name = "node6";
            this.node6.Size = new System.Drawing.Size(86, 37);
            this.node6.TabIndex = 11;
            this.node6.Text = "Node 6";
            this.node6.UseVisualStyleBackColor = true;
            this.node6.Click += new System.EventHandler(this.button6_Click);
            // 
            // node7
            // 
            this.node7.Location = new System.Drawing.Point(527, 255);
            this.node7.Name = "node7";
            this.node7.Size = new System.Drawing.Size(86, 37);
            this.node7.TabIndex = 16;
            this.node7.Text = "Node 7";
            this.node7.UseVisualStyleBackColor = true;
            this.node7.Click += new System.EventHandler(this.button3_Click_2);
            // 
            // node8
            // 
            this.node8.Location = new System.Drawing.Point(527, 298);
            this.node8.Name = "node8";
            this.node8.Size = new System.Drawing.Size(86, 37);
            this.node8.TabIndex = 17;
            this.node8.Text = "Node 8";
            this.node8.UseVisualStyleBackColor = true;
            this.node8.Click += new System.EventHandler(this.button8_Click_1);
            // 
            // node9
            // 
            this.node9.Location = new System.Drawing.Point(527, 341);
            this.node9.Name = "node9";
            this.node9.Size = new System.Drawing.Size(86, 37);
            this.node9.TabIndex = 18;
            this.node9.Text = "Node 9";
            this.node9.UseVisualStyleBackColor = true;
            this.node9.Click += new System.EventHandler(this.button9_Click_1);
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(12, 56);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(103, 42);
            this.button10.TabIndex = 19;
            this.button10.Text = "Node Type ON";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(10, 118);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(103, 45);
            this.button11.TabIndex = 20;
            this.button11.Text = "Node Sensor Set";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(25, 170);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(86, 50);
            this.button12.TabIndex = 21;
            this.button12.Text = "HI Trip Point - Action OFF";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // button13
            // 
            this.button13.Location = new System.Drawing.Point(25, 226);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(86, 48);
            this.button13.TabIndex = 22;
            this.button13.Text = "LOW Trip Point Action  OFF";
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Click += new System.EventHandler(this.button13_Click);
            // 
            // button14
            // 
            this.button14.Location = new System.Drawing.Point(9, 293);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(102, 49);
            this.button14.TabIndex = 23;
            this.button14.Text = "Pair Node OFF";
            this.button14.UseVisualStyleBackColor = true;
            this.button14.Click += new System.EventHandler(this.button14_Click);
            // 
            // button15
            // 
            this.button15.BackColor = System.Drawing.Color.LightCoral;
            this.button15.Location = new System.Drawing.Point(9, 348);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(103, 34);
            this.button15.TabIndex = 24;
            this.button15.Text = "Send Program Setup";
            this.button15.UseVisualStyleBackColor = false;
            this.button15.Click += new System.EventHandler(this.button15_Click);
            // 
            // button16
            // 
            this.button16.BackColor = System.Drawing.Color.Yellow;
            this.button16.Location = new System.Drawing.Point(12, 399);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(73, 35);
            this.button16.TabIndex = 25;
            this.button16.Text = "Timer Mode OFF";
            this.button16.UseVisualStyleBackColor = false;
            this.button16.Click += new System.EventHandler(this.button16_Click);
            // 
            // button17
            // 
            this.button17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.button17.Location = new System.Drawing.Point(12, 453);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(101, 48);
            this.button17.TabIndex = 26;
            this.button17.Text = "Go";
            this.button17.UseVisualStyleBackColor = false;
            this.button17.Click += new System.EventHandler(this.button17_Click);
            // 
            // button18
            // 
            this.button18.BackColor = System.Drawing.Color.DarkTurquoise;
            this.button18.Location = new System.Drawing.Point(527, 399);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(86, 48);
            this.button18.TabIndex = 27;
            this.button18.Text = "ON";
            this.button18.UseVisualStyleBackColor = false;
            this.button18.Click += new System.EventHandler(this.button18_Click);
            // 
            // button20
            // 
            this.button20.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button20.Location = new System.Drawing.Point(527, 453);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(86, 47);
            this.button20.TabIndex = 28;
            this.button20.Text = "OFF";
            this.button20.UseVisualStyleBackColor = false;
            this.button20.Click += new System.EventHandler(this.button20_Click);
            // 
            // GetSensorReading
            // 
            this.GetSensorReading.BackColor = System.Drawing.Color.LightCoral;
            this.GetSensorReading.Location = new System.Drawing.Point(374, 5);
            this.GetSensorReading.Name = "GetSensorReading";
            this.GetSensorReading.Size = new System.Drawing.Size(129, 34);
            this.GetSensorReading.TabIndex = 29;
            this.GetSensorReading.Text = "Get Sensor Readings";
            this.GetSensorReading.UseVisualStyleBackColor = false;
            this.GetSensorReading.Click += new System.EventHandler(this.button3_Click_3);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.button1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.button1.Location = new System.Drawing.Point(25, 633);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 55);
            this.button1.TabIndex = 30;
            this.button1.Text = "15 Seconds";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.button3.Location = new System.Drawing.Point(123, 633);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 55);
            this.button3.TabIndex = 31;
            this.button3.Text = "1 Minute";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click_4);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.button4.Location = new System.Drawing.Point(224, 633);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 55);
            this.button4.TabIndex = 32;
            this.button4.Text = "1 Hour";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click_1);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.button5.Location = new System.Drawing.Point(327, 633);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 55);
            this.button5.TabIndex = 33;
            this.button5.Text = "3 hour";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click_1);
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.button6.Location = new System.Drawing.Point(428, 633);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(75, 55);
            this.button6.TabIndex = 34;
            this.button6.Text = "6 Hours";
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button6_Click_1);
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.GreenYellow;
            this.button7.Location = new System.Drawing.Point(527, 538);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(75, 55);
            this.button7.TabIndex = 39;
            this.button7.Text = "Follow the Sun";
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.button7_Click_1);
            // 
            // button8
            // 
            this.button8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.button8.Location = new System.Drawing.Point(327, 538);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(75, 55);
            this.button8.TabIndex = 38;
            this.button8.Text = "Actuator 3/4";
            this.button8.UseVisualStyleBackColor = false;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button9
            // 
            this.button9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.button9.Location = new System.Drawing.Point(224, 538);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(75, 55);
            this.button9.TabIndex = 37;
            this.button9.Text = "Actuator 1/2";
            this.button9.UseVisualStyleBackColor = false;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // button19
            // 
            this.button19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.button19.Location = new System.Drawing.Point(123, 538);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(75, 55);
            this.button19.TabIndex = 36;
            this.button19.Text = "Actuator 1/4";
            this.button19.UseVisualStyleBackColor = false;
            this.button19.Click += new System.EventHandler(this.button19_Click);
            // 
            // button21
            // 
            this.button21.BackColor = System.Drawing.Color.PaleGreen;
            this.button21.ForeColor = System.Drawing.SystemColors.ControlText;
            this.button21.Location = new System.Drawing.Point(25, 538);
            this.button21.Name = "button21";
            this.button21.Size = new System.Drawing.Size(75, 55);
            this.button21.TabIndex = 35;
            this.button21.Text = "Motor Full Reverse";
            this.button21.UseVisualStyleBackColor = false;
            this.button21.Click += new System.EventHandler(this.button21_Click);
            // 
            // button22
            // 
            this.button22.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.button22.Location = new System.Drawing.Point(428, 538);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(75, 55);
            this.button22.TabIndex = 40;
            this.button22.Text = "Actuator Full";
            this.button22.UseVisualStyleBackColor = false;
            this.button22.Click += new System.EventHandler(this.button22_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(625, 712);
            this.Controls.Add(this.button22);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button19);
            this.Controls.Add(this.button21);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.GetSensorReading);
            this.Controls.Add(this.button20);
            this.Controls.Add(this.button18);
            this.Controls.Add(this.button17);
            this.Controls.Add(this.button16);
            this.Controls.Add(this.button15);
            this.Controls.Add(this.button14);
            this.Controls.Add(this.button13);
            this.Controls.Add(this.button12);
            this.Controls.Add(this.button11);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.node9);
            this.Controls.Add(this.node8);
            this.Controls.Add(this.node7);
            this.Controls.Add(this.node6);
            this.Controls.Add(this.node5);
            this.Controls.Add(this.node4);
            this.Controls.Add(this.node3);
            this.Controls.Add(this.node2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.node1);
            this.Controls.Add(this.buttonStart);
            this.Controls.Add(this.button2);
            this.Name = "Form1";
            this.Text = "Cool Nodes Control Panel";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button buttonStart;
        private System.Windows.Forms.Button node1;
        private System.IO.Ports.SerialPort serialPort1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button node2;
        private System.Windows.Forms.Button node3;
        private System.Windows.Forms.Button node4;
        private System.Windows.Forms.Button node5;
        private System.Windows.Forms.Button node6;
        private System.Windows.Forms.Button node7;
        private System.Windows.Forms.Button node8;
        private System.Windows.Forms.Button node9;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button button20;
        private System.Windows.Forms.Button GetSensorReading;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.Button button21;
        private System.Windows.Forms.Button button22;

        public System.Windows.Forms.KeyPressEventHandler textBox1_KeyPress_1 { get; set; }
    }
}

