﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SimpleIO;


namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        string RxString;


        public Form1()
        {

            const uint mcp2200_VID = 0x04D8;
            const uint mcp2200_PID = 0x00DF;


            SimpleIOClass.InitMCP2200(mcp2200_VID, mcp2200_PID);  // initialize the USB DEVICE
            SimpleIOClass.ConfigureIO(0);
            
              InitializeComponent(); // initialize the form
              button2.Text = "BUZZER OFF"; SimpleIOClass.ClearPin(7);
              button10.Text = "Node Type OFF"; SimpleIOClass.ClearPin(1);
              button11.Text = "Node Sensor Set"; SimpleIOClass.ClearPin(2);
              button12.Text = "HI Trip Point - Action OFF";SimpleIOClass.ClearPin(3);
              button13.Text = "LOW Trip Point Action OFF"; SimpleIOClass.ClearPin(4);
              button14.Text = "Pair Node OFF"; SimpleIOClass.ClearPin(5);
              button16.Text = "Timer Mode OFF"; SimpleIOClass.ClearPin(6);

              
        }

        private void button1_Click(object sender, EventArgs e)
        {
          
            bool isConnected = false;
            isConnected = SimpleIOClass.IsConnected();
            
            if (button1.Text == "Click to Enable")
            {

                button1.Text = "ON and Armed";

                SimpleIOClass.SetPin(6);

            }
            else
            {
                button1.Text = "Click to Enable";
                SimpleIOClass.ClearPin(6);
            }
            
        }


        private void button2_Click_1(object sender, EventArgs e)
        {
            bool isConnected = false;
            isConnected = SimpleIOClass.IsConnected();

            if (button2.Text == "BUZZER OFF")
            {
                button2.Text = "BUZZER ON";

                SimpleIOClass.SetPin(7);

            }
            else
            {
                button2.Text = "BUZZER OFF";
                SimpleIOClass.ClearPin(7);
            }
        }

        private void buttonStart_Click(object sender, EventArgs e)
        {
            serialPort1.PortName = "COM5";
            serialPort1.BaudRate = 19200;

            serialPort1.Open();
            if (serialPort1.IsOpen)
            {
                buttonStart.Enabled = false;
                node1.Enabled = true;
                textBox1.ReadOnly = false;
            }
        }

        private void buttonStop_Click(object sender, EventArgs e)
        {
           
           
            if (serialPort1.IsOpen)
            {
                char[] buff = new char[1];

                // Load element 0 with the key character.

                buff[0] ='1';

                // Send the one character buffer.
                serialPort1.Write(buff, 0, 1);
            }
        }
        private void button3_Click(object sender, EventArgs e)
        {

            if (serialPort1.IsOpen)
            {
                char[] buff = new char[1];

                // Load element 0 with the key character.

                buff[0] = '2';

                // Send the one character buffer.
                serialPort1.Write(buff, 0, 1);
            }
        }
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (serialPort1.IsOpen) serialPort1.Close();
        }
        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            // If the port is closed, don't try to send a character.

            if (!serialPort1.IsOpen) return;

            // If the port is Open, declare a char[] array with one element.
            char[] buff = new char[1];

            // Load element 0 with the key character.

            buff[0] = e.KeyChar;

            // Send the one character buffer.
            serialPort1.Write(buff, 0, 1);

            // Set the KeyPress event as handled so the character won't
            // display locally. If you want it to display, omit the next line.
            //e.Handled = true;
        }
  
        private void DisplayText(object sender, EventArgs e)
        {
            textBox1.AppendText(RxString);
        }
  
        private void serialPort1_DataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            RxString = serialPort1.ReadExisting();
            this.Invoke(new EventHandler(DisplayText));
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            if (serialPort1.IsOpen)
            {
                char[] buff = new char[1];

                // Load element 0 with the key character.

                buff[0] = '3';

                // Send the one character buffer.
                serialPort1.Write(buff, 0, 1);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (serialPort1.IsOpen)
            {
                char[] buff = new char[1];

                // Load element 0 with the key character.

                buff[0] = '4';

                // Send the one character buffer.
                serialPort1.Write(buff, 0, 1);
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (serialPort1.IsOpen)
            {
                char[] buff = new char[1];

                // Load element 0 with the key character.

                buff[0] = '5';

                // Send the one character buffer.
                serialPort1.Write(buff, 0, 1);
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (serialPort1.IsOpen)
       
            {
                char[] buff = new char[1];

                // Load element 0 with the key character.

                buff[0] = '6';

                // Send the one character buffer.
                serialPort1.Write(buff, 0, 1);
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            if (serialPort1.IsOpen)
            {
                char[] buff = new char[1];

                // Load element 0 with the key character.

                buff[0] = '7';

                // Send the one character buffer.
                serialPort1.Write(buff, 0, 1);
            }
        }





        private void button3_Click_2(object sender, EventArgs e)
        {
            if (serialPort1.IsOpen)
      
                {
                    char[] buff = new char[1];

                    // Load element 0 with the key character.

                    buff[0] = '7';

                    // Send the one character buffer.
                    serialPort1.Write(buff, 0, 1);
                }
        }

        private void button8_Click_1(object sender, EventArgs e)
        {
            if (serialPort1.IsOpen)
          
                {
                    char[] buff = new char[1];

                    // Load element 0 with the key character.

                    buff[0] = '8';

                    // Send the one character buffer.
                    serialPort1.Write(buff, 0, 1);
                }
        }

        private void button9_Click_1(object sender, EventArgs e)
        {
            if (serialPort1.IsOpen)
         
                {
                    char[] buff = new char[1];

                    // Load element 0 with the key character.

                    buff[0] = '9';

                    // Send the one character buffer.
                    serialPort1.Write(buff, 0, 1);
                }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            bool isConnected = false;
            isConnected = SimpleIOClass.IsConnected();

            if (button10.Text == "Node Type OFF")
            {

                button10.Text = "Node Type ON";

                SimpleIOClass.SetPin(1);

            }
            else
            {
                button10.Text = "Node Type OFF";
                SimpleIOClass.ClearPin(1);
            }
        }

        private void button11_Click(object sender, EventArgs e)
        {
            bool isConnected = false;
            isConnected = SimpleIOClass.IsConnected();

            if (button11.Text == "Node Sensor Set")
            {

                button11.Text = "Light Sensor Set";

                SimpleIOClass.SetPin(2);

            }
            else
            {
                button11.Text = "Node Sensor Set";
                SimpleIOClass.ClearPin(2);
            }
        }

        private void button12_Click(object sender, EventArgs e)
        {
            bool isConnected = false;
            isConnected = SimpleIOClass.IsConnected();

            if (button12.Text == "HI Trip Point - Action OFF")
            {

                button12.Text = "HI Trip Point - Action ON";

                SimpleIOClass.SetPin(3);

            }
            else
            {
                button12.Text = "HI Trip Point - Action OFF";
                SimpleIOClass.ClearPin(3);
            }
        }

        private void button13_Click(object sender, EventArgs e)
        {
            bool isConnected = false;
            isConnected = SimpleIOClass.IsConnected();

            if (button13.Text == "LOW Trip Point Action OFF")
            {

                button13.Text = "LOW Trip Point Action ON";

                SimpleIOClass.SetPin(4);

            }
            else
            {
                button13.Text = "LOW Trip Point Action OFF";
                SimpleIOClass.ClearPin(4);
            }
        }

        private void button14_Click(object sender, EventArgs e)
        {
            bool isConnected = false;
            isConnected = SimpleIOClass.IsConnected();

            if (button14.Text == "Pair Node OFF")
            {

                button14.Text = "Pair Node ON";

                SimpleIOClass.SetPin(5);

            }
            else
            {
                button14.Text = "Pair Node OFF";
                SimpleIOClass.ClearPin(5);
            }
        }

        private void button15_Click(object sender, EventArgs e)
        {
            if (serialPort1.IsOpen)
            {
                char[] buff = new char[1];

                // Load element 0 with the key character.

                buff[0] = 'C';  // 0x43 

                // Send the one character buffer.
                serialPort1.Write(buff, 0, 1);
            }
        }

        private void button16_Click(object sender, EventArgs e)
        {
            bool isConnected = false;
            isConnected = SimpleIOClass.IsConnected();

            if (button16.Text == "Timer Mode OFF")
            {

                button16.Text = "Timer Mode ON";

                SimpleIOClass.SetPin(6);

            }
            else
            {
                button16.Text = "Timer Mode OFF";
                SimpleIOClass.ClearPin(6);
            }
        }

        private void button17_Click(object sender, EventArgs e)
        {
            if (serialPort1.IsOpen)
            {
                char[] buff = new char[1];

                // Load element 0 with the key character.

                buff[0] = 'A';  // 0x41

                // Send the one character buffer.
                serialPort1.Write(buff, 0, 1);
            }
        }

        private void button18_Click(object sender, EventArgs e)
        {
                        if (serialPort1.IsOpen)
         
                {
                    char[] buff = new char[1];

                    // Load element 0 with the key character.

                    buff[0] = 'O';  //Send the ON command 

                    // Send the one character buffer.
                    serialPort1.Write(buff, 0, 1);
                }
        }

        private void button20_Click(object sender, EventArgs e)
        {
                                if (serialPort1.IsOpen)
         
                {
                    char[] buff = new char[1];

                    // Load element 0 with the key character.

                    buff[0] = 'F';  //Send the OFF command

                    // Send the one character buffer.
                    serialPort1.Write(buff, 0, 1);
                }
        }

        private void button3_Click_3(object sender, EventArgs e)
        {
            if (serialPort1.IsOpen)
            {
                char[] buff = new char[1];

                // Load element 0 with the key character.

                buff[0] = 'S';

                // Send the one character buffer.
                serialPort1.Write(buff, 0, 1);
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            SimpleIOClass.SetPin(1);
            SimpleIOClass.ClearPin(2);
            SimpleIOClass.ClearPin(3);
            SimpleIOClass.ClearPin(4);
            SimpleIOClass.ClearPin(5);
        }

        private void button3_Click_4(object sender, EventArgs e)
        {
            SimpleIOClass.SetPin(2);
            SimpleIOClass.ClearPin(1);
            SimpleIOClass.ClearPin(3);
            SimpleIOClass.ClearPin(4);
            SimpleIOClass.ClearPin(5);
        }

        private void button4_Click_1(object sender, EventArgs e)
        {
            SimpleIOClass.SetPin(3);
            SimpleIOClass.ClearPin(2);
            SimpleIOClass.ClearPin(1);
            SimpleIOClass.ClearPin(4);
            SimpleIOClass.ClearPin(5);
        }

        private void button5_Click_1(object sender, EventArgs e)
        {
            SimpleIOClass.SetPin(4);
            SimpleIOClass.ClearPin(2);
            SimpleIOClass.ClearPin(3);
            SimpleIOClass.ClearPin(1);
            SimpleIOClass.ClearPin(5);
        }

        private void button6_Click_1(object sender, EventArgs e)
        {
            SimpleIOClass.SetPin(5);
            SimpleIOClass.ClearPin(2);
            SimpleIOClass.ClearPin(3);
            SimpleIOClass.ClearPin(4);
            SimpleIOClass.ClearPin(1);
        }

        private void button21_Click(object sender, EventArgs e)
        {
            if (serialPort1.IsOpen)
            {
                char[] buff = new char[1];

                // Load element 0 with the key character.

                buff[0] = 'I';  //Send the OFF command

                // Send the one character buffer.
                serialPort1.Write(buff, 0, 1);
            }
        }


        private void button19_Click(object sender, EventArgs e)
        {
            if (serialPort1.IsOpen)
            {
                char[] buff = new char[1];

                // Load element 0 with the key character.

                buff[0] = 'J';  //Send the OFF command

                // Send the one character buffer.
                serialPort1.Write(buff, 0, 1);
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            if (serialPort1.IsOpen)
            {
                char[] buff = new char[1];

                // Load element 0 with the key character.

                buff[0] = 'K';  //Send the OFF command

                // Send the one character buffer.
                serialPort1.Write(buff, 0, 1);
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            if (serialPort1.IsOpen)
            {
                char[] buff = new char[1];

                // Load element 0 with the key character.

                buff[0] = 'L';  //Send the OFF command

                // Send the one character buffer.
                serialPort1.Write(buff, 0, 1);
            }
        }
        private void button22_Click(object sender, EventArgs e)
        {
            if (serialPort1.IsOpen)
            {
                char[] buff = new char[1];

                // Load element 0 with the key character.

                buff[0] = 'M';  //Send the OFF command

                // Send the one character buffer.
                serialPort1.Write(buff, 0, 1);
            }
        }
        
        private void button7_Click_1(object sender, EventArgs e)
        {
            if (serialPort1.IsOpen)
            {
                char[] buff = new char[1];

                // Load element 0 with the key character.

                buff[0] = 'N';  //Send the OFF command

                // Send the one character buffer.
                serialPort1.Write(buff, 0, 1);
            }
        }

        private void button23_Click(object sender, EventArgs e)
        {

        }






    }
}
